CREATE TABLE IF NOT EXISTS `role` (
    `id`    INT             UNSIGNED    NOT NULL    AUTO_INCREMENT  COMMENT 'Индетефикатор роли.',
    `name`  VARCHAR(255)                NOT NULL                    COMMENT 'Имя роли.',

    PRIMARY KEY (`id`),
    UNIQUE (`name`)
)
ENGINE          = InnoDB
DEFAULT CHARSET = utf8
COLLATE         = utf8_unicode_ci
COMMENT         = 'Список ролей пользователей.';

INSERT INTO `role`
(`name`)
VALUES
('USER');