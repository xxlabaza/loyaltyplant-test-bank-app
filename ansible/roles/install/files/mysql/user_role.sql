CREATE TABLE IF NOT EXISTS `user_role` (
    `user`  INT    UNSIGNED NOT NULL    COMMENT 'Индетефикатор пользователя.',
    `role`  INT    UNSIGNED NOT NULL    COMMENT 'Индетефикатор роли.',

    PRIMARY KEY (`user`, `role`),
    FOREIGN KEY (`user`)
        REFERENCES `user`(`id`)
        ON UPDATE RESTRICT ON DELETE CASCADE,
    FOREIGN KEY (`role`)
        REFERENCES `role`(`id`)
        ON UPDATE RESTRICT ON DELETE CASCADE
)
ENGINE          = InnoDB
DEFAULT CHARSET = utf8
COLLATE         = utf8_unicode_ci
COMMENT         = 'Связи пользователь->роли.';