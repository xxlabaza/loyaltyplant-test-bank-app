CREATE TABLE IF NOT EXISTS `account` (
    `id`            INT             UNSIGNED    NOT NULL    AUTO_INCREMENT  COMMENT 'Индетефикатор счёта.',
    `user`          INT             UNSIGNED    NOT NULL                    COMMENT 'Индетефикатор пользователя, владельца счёта.',
    `name`          VARCHAR(255)                NOT NULL                    COMMENT 'Имя счёта.',
    `amount`        DECIMAL(10,2)   UNSIGNED    NOT NULL                    COMMENT 'Сумма на счету пользователя.',

    PRIMARY KEY (`id`),
    FOREIGN KEY (`user`)
        REFERENCES `user`(`id`)
        ON UPDATE RESTRICT ON DELETE CASCADE
)
ENGINE          = InnoDB
DEFAULT CHARSET = utf8
COLLATE         = utf8_unicode_ci
COMMENT         = 'Список счетов пользователей в системе.';