CREATE TABLE IF NOT EXISTS `account_history` (
    `id`                INT             UNSIGNED    NOT NULL    AUTO_INCREMENT              COMMENT 'Индетефикатор изменения.',
    `account`           INT             UNSIGNED    NOT NULL                                COMMENT 'Индетефикатор счёта, с которым проводится операция.',
    `amount`            DECIMAL(10,2)   UNSIGNED    NOT NULL                                COMMENT 'Сумма, на которую изменяется счёт.',
    `time`              TIMESTAMP                   NOT NULL    DEFAULT CURRENT_TIMESTAMP   COMMENT 'Время изменения счёта.',
    `partner_account`   INT             UNSIGNED    NULL                                    COMMENT 'Индетефикатор счёта пользователя, участника операции. Значение может совпадать со значением из колонки `amount`.',
    `operation`         ENUM(
                            'CREATE',
                            'DEBIT', 
                            'DEPOSIT', 
                            'TRANSFER'
                        )                           NOT NULL                                COMMENT 'Тип операции над счётом.',

    PRIMARY KEY (`id`),
    FOREIGN KEY (`account`)
        REFERENCES `account`(`id`)
        ON UPDATE RESTRICT ON DELETE CASCADE,
    FOREIGN KEY (`partner_account`)
        REFERENCES `account`(`id`)
        ON UPDATE RESTRICT ON DELETE SET NULL
)
ENGINE          = InnoDB
DEFAULT CHARSET = utf8
COLLATE         = utf8_unicode_ci
COMMENT         = 'История счетов в системе.';