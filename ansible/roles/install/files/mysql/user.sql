CREATE TABLE IF NOT EXISTS `user` (
    `id`            INT             UNSIGNED    NOT NULL    AUTO_INCREMENT  COMMENT 'Индетефикатор пользователя.',
    `username`      VARCHAR(25)                 NOT NULL                    COMMENT 'Логин пользователя.',
    `password`      CHAR(64)                    NOT NULL                    COMMENT 'Пароль пользователя.',
    `first_name`    VARCHAR(25)                 NOT NULL                    COMMENT 'Имя пользователя.',
    `middle_name`   VARCHAR(25)                 NOT NULL                    COMMENT 'Отчество пользователя.',
    `last_name`     VARCHAR(25)                 NOT NULL                    COMMENT 'Фамилия пользователя.',
    `is_active`     BOOLEAN                     NOT NULL    DEFAULT TRUE    COMMENT 'Флаг, указывающий - активен пользователь в системе или нет (удалил своё аккаунт).',

    PRIMARY KEY (`id`),
    UNIQUE (`username`),
    UNIQUE (`first_name`, `middle_name`, `last_name`)
)
ENGINE          = InnoDB
DEFAULT CHARSET = utf8
COLLATE         = utf8_unicode_ci
COMMENT         = 'Список пользователей платёжной системы.';