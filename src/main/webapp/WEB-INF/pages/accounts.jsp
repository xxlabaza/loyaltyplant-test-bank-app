<%--
    Document   : accounts
    Created on : Aug 25, 2014, 5:48:57 PM
    Author     : xxlabaza
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@page session="true" contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 3.1.3
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>Bank App | User bank accounts</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/font-awesome/css/font-awesome.min.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/bootstrap/css/bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/uniform/css/uniform.default.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/jquery-notific8/jquery.notific8.min.css"/>
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN THEME STYLES -->
<link rel="stylesheet" type="text/css" href="assets/global/css/components.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/css/plugins.css"/>
<link rel="stylesheet" type="text/css" href="assets/admin/layout/css/layout.css"/>
<link rel="stylesheet" type="text/css" href="assets/admin/layout/css/themes/darkblue.css" id="style_color"/>
<link rel="stylesheet" type="text/css" href="assets/admin/layout/css/custom.css"/>
<!-- END THEME STYLES -->
<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed page-sidebar-fixed page-boxed page-footer-fixed">
<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner container">
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            <img src="assets/admin/layout/img/logo.png" alt="logo" class="logo-default"/>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN TOP NAVIGATION MENU -->
        <div class="top-menu">
            <ul class="nav navbar-nav pull-right">
                <!-- BEGIN USER LOGIN DROPDOWN -->
                <li class="dropdown dropdown-user">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                    <span class="username username-hide-on-mobile">
                    ${currentUser.firstName} ${currentUser.lastName} </span>
                    <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#edit_profile" data-toggle="modal">
                            <i class="icon-user"></i> My Profile </a>
                        </li>
                        <li class="divider">
                        </li>
                        <li>
                            <a href="javascript:formSubmit()">
                            <i class="icon-key"></i> Log Out </a>
                        </li>
                    </ul>
                </li>
                <c:url value="j_spring_security_logout" var="logoutUrl" />
                <form action="${logoutUrl}" method="post" id="logoutForm">
                </form>
                <script>
                    function formSubmit() {
                        document.getElementById("logoutForm").submit();
                    }
                </script>
                <!-- END USER LOGIN DROPDOWN -->
            </ul>
        </div>
        <!-- END TOP NAVIGATION MENU -->
    </div>
    <!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
<div class="clearfix">
</div>
<div class="container">
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
        <div class="page-sidebar-wrapper">
            <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
            <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
            <div class="page-sidebar navbar-collapse collapse">
                <!-- BEGIN SIDEBAR MENU -->
                <ul class="page-sidebar-menu " data-auto-scroll="true" data-slide-speed="200">
                    <li class="start active open">
                        <a href="javascript:;">
                        <i class="icon-credit-card"></i>
                        <span class="title">My Bank Accounts</span>
                        <span class="selected"></span>
                        </a>
                    </li>
                </ul>
                <!-- END SIDEBAR MENU -->
            </div>
        </div>
        <!-- END SIDEBAR -->
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <!-- BEGIN STYLE CUSTOMIZER -->
                <div class="theme-panel hidden-xs hidden-sm">
                    <div class="toggler">
                    </div>
                    <div class="toggler-close">
                    </div>
                    <div class="theme-options">
                        <div class="theme-option theme-colors clearfix">
                            <span>
                            THEME COLOR </span>
                            <ul>
                                <li class="color-default tooltips" data-style="default" data-container="body" data-original-title="Default">
                                </li>
                                <li class="color-darkblue current tooltips" data-style="darkblue" data-container="body" data-original-title="Dark Blue">
                                </li>
                                <li class="color-blue tooltips" data-style="blue" data-container="body" data-original-title="Blue">
                                </li>
                                <li class="color-grey tooltips" data-style="grey" data-container="body" data-original-title="Grey">
                                </li>
                                <li class="color-light tooltips" data-style="light" data-container="body" data-original-title="Light">
                                </li>
                                <li class="color-light2 tooltips" data-style="light2" data-container="body" data-html="true" data-original-title="Light 2">
                                </li>
                            </ul>
                        </div>
                        <div class="theme-option">
                            <span>
                            Layout </span>
                            <select class="layout-option form-control input-small">
                                <option value="fluid">Fluid</option>
                                <option value="boxed" selected="selected">Boxed</option>
                            </select>
                        </div>
                        <div class="theme-option">
                            <span>
                            Header </span>
                            <select class="page-header-option form-control input-small">
                                <option value="fixed" selected="selected">Fixed</option>
                                <option value="default">Default</option>
                            </select>
                        </div>
                        <div class="theme-option">
                            <span>
                            Sidebar Mode</span>
                            <select class="sidebar-option form-control input-small">
                                <option value="fixed" selected="selected">Fixed</option>
                                <option value="default">Default</option>
                            </select>
                        </div>
                        <div class="theme-option">
                            <span>
                            Sidebar Menu </span>
                            <select class="sidebar-menu-option form-control input-small">
                                <option value="accordion" selected="selected">Accordion</option>
                                <option value="hover">Hover</option>
                            </select>
                        </div>
                        <div class="theme-option">
                            <span>
                            Sidebar Style </span>
                            <select class="sidebar-style-option form-control input-small">
                                <option value="default" selected="selected">Default</option>
                                <option value="light">Light</option>
                            </select>
                        </div>
                        <div class="theme-option">
                            <span>
                            Sidebar Position </span>
                            <select class="sidebar-pos-option form-control input-small">
                                <option value="left" selected="selected">Left</option>
                                <option value="right">Right</option>
                            </select>
                        </div>
                        <div class="theme-option">
                            <span>
                            Footer </span>
                            <select class="page-footer-option form-control input-small">
                                <option value="fixed" selected="selected">Fixed</option>
                                <option value="default">Default</option>
                            </select>
                        </div>
                    </div>
                </div>
                <!-- END STYLE CUSTOMIZER -->
                <!-- BEGIN PAGE HEADER-->
                <h3 class="page-title">
                Bank Accounts <small>user's bank accounts information</small>
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="accounts">Home</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <i class="icon-credit-card"></i>
                            <a href="accounts">My Bank Accounts</a>
                        </li>
                    </ul>
                </div>
                <!-- END PAGE HEADER-->
                <!-- BEGIN PAGE CONTENT-->
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN PORTLET -->
                        <div class="portlet box blue-madison">
                            <!-- BEGID PORTLET TITLE -->
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-search"></i>Detailed information about user's accounts
                                </div>
                                <div class="actions">
                                    <a href="#add_account" data-toggle="modal" class="btn btn-default btn-sm">
                                        <i class="fa fa-plus"></i> Add
                                    </a>
                                    <div class="btn-group">
                                        <a class="btn btn-default btn-sm" href="#" data-toggle="dropdown">
                                        <i class="fa fa-bank"></i> Actions <i class="fa fa-angle-down"></i>
                                        </a>
                                        <ul class="dropdown-menu pull-right">
                                            <li>
                                                <a href="#refil" data-toggle="modal">
                                                    <i class="fa fa-plus"></i> Deposit
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#transfer" data-toggle="modal">
                                                    <i class="fa fa-paper-plane-o"></i> Transfer
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#debit" data-toggle="modal">
                                                    <i class="fa fa-minus"></i> Debit
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- END PORTLET TITLE -->
                            <!-- BEGIN PORTLET BODY -->
                            <div class="portlet-body">
                                <table class="table table-striped table-bordered table-hover" id="accounts_table">
                                    <thead>
                                        <tr>
                                            <th>
                                                Account name
                                            </th>
                                            <th width="30%">
                                                Amount
                                            </th>
                                            <th>
                                                &nbsp;
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:if test="${currentUser.accounts != null && !currentUser.accounts.isEmpty()}">
                                            <c:forEach items="${currentUser.accounts}" var="account">
                                                <tr>
                                                    <td data-account-id="${account.id}">
                                                        ${account.name}
                                                    </td>
                                                    <td>
                                                        ${account.amount}
                                                    </td>
                                                    <td>
                                                        <a href="accounts/${account.id}" class="btn default btn-xs black">
                                                            <i class="fa fa-trash-o"></i> Delete
                                                        </a>
                                                    </td>
                                                </tr>
                                            </c:forEach>
                                        </c:if>
                                    </tbody>
                                </table>
                                <!-- BEGIN EDIT PROFILE MODAL -->
                                <div class="modal fade bs-modal-sm" id="edit_profile" tabindex="-1" role="basic" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">Editing profile</h4>
                                            </div>
                                            <div class="modal-body form">
                                                <form:form id="editProfileForm" action="accounts/edit_user" method="POST" class="form-horizontal form-row-seperated">
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <div class="input-group">
                                                                <span class="input-group-addon">
                                                                    <i class="icon-pencil"></i>
                                                                </span>
                                                                <input name="firstName" type="text" class="form-control" placeholder="First name" value="${currentUser.firstName}">
                                                            </div>
                                                            <span class="help-block">Your first name</span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <div class="input-group">
                                                                <span class="input-group-addon">
                                                                    <i class="icon-pencil"></i>
                                                                </span>
                                                                <input name="middleName" type="text" class="form-control" placeholder="Middle name" value="${currentUser.middleName}">
                                                            </div>
                                                            <span class="help-block">Your middle name</span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <div class="input-group">
                                                                <span class="input-group-addon">
                                                                    <i class="icon-pencil"></i>
                                                                </span>
                                                                <input name="lastName" type="text" class="form-control" placeholder="Last name" value="${currentUser.lastName}">
                                                            </div>
                                                            <span class="help-block">Your last name</span>
                                                        </div>
                                                    </div>
                                                    <input type="hidden" name="id" value="${currentUser.id}" />
                                                </form:form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn default" data-dismiss="modal">Close</button>
                                                <button type="button" class="btn blue" onclick="$('#editProfileForm').submit()">Save changes</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- END EDIT PROFILE MODAL -->
                                <!-- BEGIN ADD ACCOUNT MODAL -->
                                <div class="modal fade bs-modal-sm" id="add_account" tabindex="-1" role="basic" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">Adding account</h4>
                                            </div>
                                            <div class="modal-body form">
                                                <form:form id="addNewAccountForm" action="accounts/add_new" class="form-horizontal form-row-seperated">
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <div class="input-group">
                                                                <span class="input-group-addon">
                                                                    <i class="icon-credit-card"></i>
                                                                </span>
                                                                <input name="name" type="text" class="form-control" placeholder="Account name">
                                                            </div>
                                                            <span class="help-block">Type new account name</span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <div class="input-group">
                                                                <input name="amount" type="text" class="amount-touchSpin" value="0" name="demo1" class="form-control">
                                                            </div>
                                                            <span class="help-block">Enter amount to add</span>
                                                        </div>
                                                    </div>
                                                </form:form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn default" data-dismiss="modal">Cancel</button>
                                                <button type="button" class="btn blue" onclick="$('#addNewAccountForm').submit()">Create account</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- END ADD ACCOUNT MODAL -->
                                <!-- BEGIN REFIL MODAL -->
                                <div class="modal fade bs-modal-sm" id="refil" tabindex="-1" role="basic" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">Operation: deposit account</h4>
                                            </div>
                                            <div class="modal-body form">
                                                <form:form id="depositAccountForm" action="accounts/deposit" method="POST" class="form-horizontal form-row-seperated">
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <div class="input-group">
                                                                <span class="input-group-addon">
                                                                    <i class="icon-credit-card"></i>
                                                                </span>
                                                                <select name="account" class="form-control select2me" data-placeholder="Select...">
                                                                    <option value=""></option>
                                                                    <c:if test="${currentUser.accounts != null && !currentUser.accounts.isEmpty()}">
                                                                        <c:forEach items="${currentUser.accounts}" var="account">
                                                                            <option value="${account.id}">${account.name}</option>
                                                                        </c:forEach>
                                                                    </c:if>
                                                                </select>
                                                            </div>
                                                            <span class="help-block">Select your account by name</span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <div class="input-group">
                                                                <input name="amount" class="amount-touchSpin" type="text" value="0" name="demo1" class="form-control">
                                                            </div>
                                                            <span class="help-block">Enter amount to deposit</span>
                                                        </div>
                                                    </div>
                                                </form:form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn default" data-dismiss="modal">Cancle</button>
                                                <button type="button" onclick="$('#depositAccountForm').submit()" class="btn blue">Deposit</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- END REFIL MODAL -->
                                <!-- BEGIN Transfer MODAL -->
                                <div class="modal fade bs-modal-sm" id="transfer" tabindex="-1" role="basic" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">Operation: money transfer</h4>
                                            </div>
                                            <div class="modal-body form">
                                                <form:form id="transferAccountForm" action="accounts/transfer" method="POST" class="form-horizontal form-row-seperated">
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <div class="input-group">
                                                                <span class="input-group-addon">
                                                                    <i class="icon-credit-card"></i>
                                                                </span>
                                                                <select name="account" class="form-control select2me" data-placeholder="Select...">
                                                                    <option value=""></option>
                                                                    <c:if test="${currentUser.accounts != null && !currentUser.accounts.isEmpty()}">
                                                                        <c:forEach items="${currentUser.accounts}" var="account">
                                                                            <option value="${account.id}">${account.name}</option>
                                                                        </c:forEach>
                                                                    </c:if>
                                                                </select>
                                                            </div>
                                                            <span class="help-block">Select your account by name</span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <div class="input-group">
                                                                <input name="amount" class="amount-touchSpin" type="text" value="0" name="demo1" class="form-control">
                                                            </div>
                                                            <span class="help-block">Enter amount to tranfer</span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <div class="input-group">
                                                                <span class="input-group-addon">
                                                                    <i class="icon-user"></i>
                                                                </span>
                                                                <select id="partnerSelection" class="form-control select2me" data-placeholder="Select...">
                                                                    <option value=""></option>
                                                                    <c:if test="${otherUsers != null && !otherUsers.isEmpty()}">
                                                                        <c:forEach items="${otherUsers}" var="partner">
                                                                            <option value="${partner.id}">${partner.lastName} ${partner.firstName} ${partner.middleName}</option>
                                                                        </c:forEach>
                                                                    </c:if>
                                                                </select>
                                                            </div>
                                                            <span class="help-block">Select user for transfer</span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <div class="input-group">
                                                                <span class="input-group-addon">
                                                                    <i class="icon-credit-card"></i>
                                                                </span>
                                                                <select id="partnerAccountSelection" name="partnerAccount" class="form-control select2me" data-placeholder="Select...">
                                                                    <option value=""></option>
                                                                </select>
                                                            </div>
                                                            <span class="help-block">Select partner account</span>
                                                        </div>
                                                    </div>
                                                </form:form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn default" data-dismiss="modal">Cancel</button>
                                                <button type="button" onclick="$('#transferAccountForm').submit()" class="btn blue">Transfer</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- END Transfer MODAL -->
                                <!-- BEGIN DEBIT MODAL -->
                                <div class="modal fade bs-modal-sm" id="debit" tabindex="-1" role="basic" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">Operation: debit account</h4>
                                            </div>
                                            <div class="modal-body form">
                                                <form:form id="debitAccountForm" action="accounts/debit" method="POST" class="form-horizontal form-row-seperated">
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <div class="input-group">
                                                                <span class="input-group-addon">
                                                                    <i class="icon-credit-card"></i>
                                                                </span>
                                                                <select name="account" class="form-control select2me" data-placeholder="Select...">
                                                                    <option value=""></option>
                                                                    <c:if test="${currentUser.accounts != null && !currentUser.accounts.isEmpty()}">
                                                                        <c:forEach items="${currentUser.accounts}" var="account">
                                                                            <option value="${account.id}">${account.name}</option>
                                                                        </c:forEach>
                                                                    </c:if>
                                                                </select>
                                                            </div>
                                                            <span class="help-block">Select your account by name</span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <div class="input-group">
                                                                <input name="amount" class="amount-touchSpin" type="text" value="0" name="demo1" class="form-control">
                                                            </div>
                                                            <span class="help-block">Enter amount to debit</span>
                                                        </div>
                                                    </div>
                                                </form:form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn default" data-dismiss="modal">Cancel</button>
                                                <button type="button" onclick="$('#debitAccountForm').submit()" class="btn blue">Debit</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- END DEBIT MODAL -->
                            </div>
                            <!-- END PORTLET BODY -->
                        </div>
                        <!-- END PORTLET -->
                    </div>
                </div>
                <!-- END PAGE CONTENT-->
            </div>
        </div>
        <!-- END CONTENT -->
    </div>
    <!-- END CONTAINER -->
    <!-- BEGIN FOOTER -->
    <div class="page-footer">
        <div class="page-footer-inner">
             2014 &copy; Bank App. LoyaltyPlant test task.
        </div>
        <div class="page-footer-tools">
            <span class="go-top">
            <i class="fa fa-angle-up"></i>
            </span>
        </div>
    </div>

    <div id="account_history_table_template" style="display: none">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>
                        <i class="fa fa-bank"></i> Operation
                    </th>
                    <th>
                        <i class="fa fa-clock-o"></i> Time
                    </th>
                    <th>
                        <i class="fa fa-money"></i> Amount
                    </th>
                    <th>
                        <i class="fa fa-user"></i> Partner
                    </th>
                    <th>
                        <i class="fa fa-credit-card"></i> <strong>Partner acount</strong>
                    </th>
                </tr>
            </thead>
            <tbody class="account_history_table_content">
            </tbody>
        </table>
        <table>
            <tr class="accountHistory-row">
            <td class="accountHistory-operation">
            </td>
            <td class="accountHistory-time">
            </td>
            <td class="accountHistory-amount">
            </td>
            <td class="accountHistory-partner">
            </td>
            <td class="accountHistory-partnerAccount">
            </td>
        </tr>
    </table>
    </div>
</div>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="assets/global/plugins/jquery-1.11.0.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="assets/global/plugins/select2/select2.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-daterangepicker/moment.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/datatables/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-notific8/jquery.notific8.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/table-advanced.js"></script>
<script>
    jQuery(document).ready(function() {
        Metronic.init(); // init metronic core components
        Layout.init(); // init current layout
        QuickSidebar.init(); // init quick sidebar
        Demo.init(); // init demo features

        initializeAccountsTable();
        $(".amount-touchSpin").TouchSpin({
            buttondown_class: 'btn green',
            buttonup_class: 'btn green',
            min: 0,
            max: 1000000000,
            stepinterval: 1,
            maxboostedstep: 10000000,
            postfix: '<i class="fa fa-rub"></i>'
        });

        var $partnerSelection = $("#partnerSelection"),
            $partnerAccountSelection = $("#partnerAccountSelection");

        $partnerAccountSelection.select2("enable", false);
        $partnerSelection.change(function () {
            var $partnerId = $partnerSelection.select2("val");
            $partnerAccountSelection.empty();
            $partnerAccountSelection.append($("<option value=\"\"/>"))
            if ($partnerId !== null && $partnerId !== "") {
                $.getJSON("accounts/user/" + $partnerId + "/all", function (data) {
                    $.each(data, function (index, value) {
                        var $option = $("<option/>");
                        $option.attr("value", value.id);
                        $option.text(value.name);
                        $partnerAccountSelection.append($option);
                    });
                    $partnerAccountSelection.select2("enable", true);
                });
            } else {
                $partnerAccountSelection.select2("val", "");
                $partnerAccountSelection.select2("enable", false);
            }
        });
    });

    function initializeAccountsTable () {
        var table = $("#accounts_table");
        /* Formatting function for row details */
        function fnFormatDetails (oTable, nTr) {
            var accountId = $(nTr).find("[data-account-id]").first().attr("data-account-id"),
                $accountHistoryTable = $("#account_history_table_template .table").first().clone(),
                $accountHistoryContent = $accountHistoryTable.find(".account_history_table_content").first(),
                $accountHistoryRowTemplate = $("#account_history_table_template .accountHistory-row").first().clone();

            $.ajax({
                url:        "accounts/" + accountId + "/history",
                dataType:   "json",
                async:      false,
                success: function (data) {
                    $.each(data, function (index, value) {
                        var $row = $accountHistoryRowTemplate.clone();
                        var operation;
                        switch (value.operation) {
                        case "CREATE":
                            operation = '<span class="label label-sm label-info">CREATE</span>';
                            break;
                        case "DEPOSIT":
                            operation = '<span class="label label-sm label-success">DEPOSIT</span>';
                            break;
                        case "TRANSFER":
                            operation = '<span class="label label-sm label-warning">TRANSFER</span>';
                            break;
                        case "DEBIT":
                            operation = '<span class="label label-sm label-danger">DEBIT</span>';
                            break;
                        }
                        $row.find(".accountHistory-operation").html(operation);
                        $row.find(".accountHistory-time").html(moment(value.time).fromNow());
                        $row.find(".accountHistory-amount").html(value.amount);
                        if (value.partnerAccount !== null) {
                            $row.find(".accountHistory-partner").html(value.partnerAccount.user.firstName + " " + value.partnerAccount.user.lastName);
                            $row.find(".accountHistory-partnerAccount").html(value.partnerAccount.name);
                        }
                        $accountHistoryContent.append($row);
                    });
                }
            });
            return $accountHistoryTable;
        }
        /*
         * Insert a 'details' column to the table
         */
        var nCloneTh = document.createElement('th');
        nCloneTh.className = "table-checkbox";

        var nCloneTd = document.createElement('td');
        nCloneTd.innerHTML = '<span class="row-details row-details-close"></span>';

        table.find('thead tr').each(function () {
            this.insertBefore(nCloneTh, this.childNodes[0]);
        });

        table.find('tbody tr').each(function () {
            this.insertBefore(nCloneTd.cloneNode(true), this.childNodes[0]);
        });
        /*
         * Initialize DataTables, with no sorting on the 'details' column
         */
        var oTable = table.dataTable({
            "columnDefs": [{
                "orderable": false,
                "targets": [0]
            }],
            "order": [
                [1, 'asc']
            ],
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,
        });
        var tableWrapper = $('#accounts_table_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper

        tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown

        /* Add event listener for opening and closing details
         * Note that the indicator for showing which row is open is not controlled by DataTables,
         * rather it is done here
         */
        table.on('click', ' tbody td .row-details', function () {
            var nTr = $(this).parents('tr')[0];
            if (oTable.fnIsOpen(nTr)) {
                /* This row is already open - close it */
                $(this).addClass("row-details-close").removeClass("row-details-open");
                oTable.fnClose(nTr);
            } else {
                /* Open this row */
                $(this).addClass("row-details-open").removeClass("row-details-close");
                oTable.fnOpen(nTr, fnFormatDetails(oTable, nTr), 'details');
            }
        });
    }

    function submitFormToServer (formId) {
        var $form = $("#" + formId),
            data = {};

        $form.serializeArray().map(function (item) {
            data[item.name] = item.value;
        });
        $form.submit(function (event) {
            // Stop form from submitting normally
            event.preventDefault();

        });
    }
</script>
<c:if test="${not empty errorMessage || not empty successMessage}">
<script>
    jQuery(document).ready(function() {
        var settings = {
            theme:          "${not empty successMessage ? "teal" : "lemon"}",
            sticky:         true,
            horizontalEdge: "top",
            verticalEdge:   "right"
        };
        $.notific8("zindex", 11500);
        $.notific8("${not empty successMessage ? successMessage : errorMessage}", settings);
    });
</script>
</c:if>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>