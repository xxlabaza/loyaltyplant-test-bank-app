package com.effylab.bank.server.dao;

import com.effylab.bank.server.model.Role;

/**
 *
 * <p>
 * @author Artem Labazin, xxlabaza@gmail.com
 * <p>
 * @since Aug 29, 2014
 * <p>
 * @version 1.0.0
 */
public interface RoleDAO extends GenericDAO<Role, Long> {

    Role getByName (String roleName);
}
