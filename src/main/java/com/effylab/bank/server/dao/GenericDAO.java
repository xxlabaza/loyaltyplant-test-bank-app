package com.effylab.bank.server.dao;

import java.io.Serializable;

/**
 *
 * <p>
 * @author Artem Labazin, xxlabaza@gmail.com
 * <p>
 * @since Aug 27, 2014
 * <p>
 * @version 1.0.0
 */
public interface GenericDAO<T, PK extends Serializable> {

    PK create (T entity);

    T read (PK id);

    void update (T entity);

    void delete (T entity);
}
