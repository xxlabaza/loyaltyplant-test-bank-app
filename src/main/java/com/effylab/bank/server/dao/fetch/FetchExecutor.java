package com.effylab.bank.server.dao.fetch;

import java.lang.reflect.Method;
import java.util.List;

/**
 *
 * <p>
 * @author Artem Labazin, xxlabaza@gmail.com
 * <p>
 * @since Aug 27, 2014
 * <p>
 * @version 1.0.0
 */
public interface FetchExecutor<T> {

    List<T> executeFind (Method method, Object[] queryArguments);

    T executeGet (Method method, Object[] queryArguments);

    boolean executeDo (Method method, Object[] queryArguments);
}
