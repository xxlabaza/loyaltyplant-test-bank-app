package com.effylab.bank.server.dao;

import com.effylab.bank.server.dao.fetch.FetchExecutor;
import com.effylab.bank.server.dao.fetch.FindNamingStrategy;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class GenericDAOImpl<T, PK extends Serializable> implements GenericDAO<T, PK>, FetchExecutor<T> {

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private FindNamingStrategy findNamingStrategy;

    private final Class<T> type;

    public GenericDAOImpl (Class<T> type) {
        this.type = type;
    }

    @Override
    @SuppressWarnings("unchecked")
    public PK create (T entity) {
        return (PK) sessionFactory.getCurrentSession().save(entity);
    }

    @Override
    @SuppressWarnings("unchecked")
    public T read (PK id) {
        return (T) sessionFactory.getCurrentSession().get(type, id);
    }

    @Override
    public void update (T entity) {
        sessionFactory.getCurrentSession().update(entity);
    }

    @Override
    public void delete (T entity) {
        sessionFactory.getCurrentSession().delete(entity);
    }

    @Override
    @SuppressWarnings({"unchecked"})
    public List<T> executeFind (Method method, Object[] queryArguments) {
        Query query = createQuery(method, queryArguments);
        return query.list();
    }

    @Override
    @SuppressWarnings("unchecked")
    public T executeGet (Method method, Object[] queryArguments) {
        Query query = createQuery(method, queryArguments);
        return (T) query.uniqueResult();
    }

    @Override
    public boolean executeDo (Method method, Object[] queryArguments) {
        Query query = createQuery(method, queryArguments);
        return query.executeUpdate() > 0;
    }

    private Query createQuery (Method method, Object[] queryArguments) {
        String queryName = findNamingStrategy.queryNameFromMethod(type, method);
        Query query = sessionFactory.getCurrentSession().getNamedQuery(queryName);
        if (queryArguments != null && queryArguments.length > 0) {
            String[] namedParameters = query.getNamedParameters();
            if (namedParameters.length == 0) {
                // Set positional arguments
                for (int index = 0; index < queryArguments.length; index++) {
                    Object queryArgument = queryArguments[index];
                    query.setParameter(index, queryArgument);
                }
            } else {
                // Set named arguments
                for (int index = 0; index < queryArguments.length; index++) {
                    Object queryArgument = queryArguments[index];
                    if (queryArgument instanceof Collection) {
                        query.setParameterList(namedParameters[index], (Collection) queryArgument);
                    } else {
                        query.setParameter(namedParameters[index], queryArgument);
                    }
                }
            }
        }
        return query;
    }
}
