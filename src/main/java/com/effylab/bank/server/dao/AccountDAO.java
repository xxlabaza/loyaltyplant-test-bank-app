package com.effylab.bank.server.dao;

import com.effylab.bank.server.model.Account;
import java.math.BigDecimal;
import java.util.List;

/**
 *
 * <p>
 * @author Artem Labazin, xxlabaza@gmail.com
 * <p>
 * @since Aug 22, 2014
 * <p>
 * @version 1.0.0
 */
public interface AccountDAO extends GenericDAO<Account, Long> {

    List<Account> findByUser (Long userId);

    boolean doDeposit (BigDecimal amount, Long accountId);

    boolean doDebit (BigDecimal amount, Long accountId);
}
