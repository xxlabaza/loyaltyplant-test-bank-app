package com.effylab.bank.server.dao;

import com.effylab.bank.server.model.User;
import java.util.List;

/**
 *
 * <p>
 * @author Artem Labazin, xxlabaza@gmail.com
 * <p>
 * @since Aug 22, 2014
 * <p>
 * @version 1.0.0
 */
public interface UserDAO extends GenericDAO<User, Long> {

    User getByUsername (String username);

    List<User> findAll ();

    boolean doDeleteByUsername (String username);
}
