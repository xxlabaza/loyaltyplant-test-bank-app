package com.effylab.bank.server.dao;

import com.effylab.bank.server.model.AccountHistory;
import java.util.List;

/**
 *
 * <p>
 * @author Artem Labazin, xxlabaza@gmail.com
 * <p>
 * @since Aug 22, 2014
 * <p>
 * @version 1.0.0
 */
public interface AccountHistoryDAO extends GenericDAO<AccountHistory, Long> {

    List<AccountHistory> findAccountHistoryByAccount (Long accountId);
}
