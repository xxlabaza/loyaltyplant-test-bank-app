package com.effylab.bank.server.dao.fetch;

import java.lang.reflect.Method;
import javax.annotation.Resource;

/**
 *
 * <p>
 * @author Artem Labazin, xxlabaza@gmail.com
 * <p>
 * @since Aug 27, 2014
 * <p>
 * @version 1.0.0
 */
@Resource
public class SimpleFindNamingStrategy implements FindNamingStrategy {

    @Override
    public String queryNameFromMethod (Class<?> type, Method method) {
        return new StringBuilder()
                .append(type.getName())
                .append('.')
                .append(method.getName())
                .toString();
    }
}
