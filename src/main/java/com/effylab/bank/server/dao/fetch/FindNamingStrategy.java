package com.effylab.bank.server.dao.fetch;

import java.lang.reflect.Method;

/**
 *
 * <p>
 * @author Artem Labazin, xxlabaza@gmail.com
 * <p>
 * @since Aug 27, 2014
 * <p>
 * @version 1.0.0
 */
public interface FindNamingStrategy {

    String queryNameFromMethod (Class<?> type, Method method);
}
