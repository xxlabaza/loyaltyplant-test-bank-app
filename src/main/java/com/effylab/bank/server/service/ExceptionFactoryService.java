package com.effylab.bank.server.service;

import com.effylab.bank.server.exception.AccountDoesntExistException;
import com.effylab.bank.server.exception.AccountInsufficientFundsException;
import com.effylab.bank.server.exception.UserAlreadyExistsException;
import com.effylab.bank.server.exception.UserDoesntExistException;
import java.math.BigDecimal;
import java.util.Locale;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

/**
 *
 * <p>
 * @author Artem Labazin, xxlabaza@gmail.com
 * <p>
 * @since Sep 3, 2014
 * <p>
 * @version 1.0.0
 */
@Service
public class ExceptionFactoryService {

    @Autowired
    private MessageSource messageSource;

    public AccountInsufficientFundsException getAccountInsufficientFundsException (BigDecimal amount) {
        String message = messageSource.getMessage("exception.accountInsufficientFundsException",
                                                  new Object[]{amount},
                                                  Locale.getDefault());
        return new AccountInsufficientFundsException(message);
    }

    public AccountDoesntExistException getAccountDoesntExistException (Long accountId) {
        String message = messageSource.getMessage("exception.accountDoesntExistException",
                                                  new Object[]{accountId},
                                                  Locale.getDefault());
        return new AccountDoesntExistException(message);
    }

    public UserAlreadyExistsException getUserAlreadyExistsException (String username) {
        String message = messageSource.getMessage("exception.userAlreadyExistsException",
                                                  new Object[]{username},
                                                  Locale.getDefault());
        return new UserAlreadyExistsException(message);
    }

    public UserDoesntExistException getUserDoesntExistException () {
        String message = messageSource.getMessage("exception.accountDoesntExistException",
                                                  new Object[]{},
                                                  Locale.getDefault());
        return new UserDoesntExistException(message);
    }
}
