package com.effylab.bank.server.service;

import com.effylab.bank.server.dao.AccountDAO;
import com.effylab.bank.server.dao.AccountHistoryDAO;
import com.effylab.bank.server.exception.AccountDoesntExistException;
import com.effylab.bank.server.exception.AccountInsufficientFundsException;
import com.effylab.bank.server.model.Account;
import com.effylab.bank.server.model.AccountHistory;
import java.math.BigDecimal;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import static org.springframework.transaction.annotation.Isolation.READ_COMMITTED;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * <p>
 * @author Artem Labazin, xxlabaza@gmail.com
 * <p>
 * @since Sep 3, 2014
 * <p>
 * @version 1.0.0
 */
@Service
@Transactional(rollbackFor = Exception.class,
               isolation = READ_COMMITTED)
public class AccountService {

    @Autowired
    private AccountDAO accountDAO;

    @Autowired
    private AccountHistoryDAO accountHistoryDAO;

    @Autowired
    private ExceptionFactoryService exceptionFactoryServic;

    @Transactional(readOnly = true)
    public List<Account> findAccountByUser (Long userId) {
        return accountDAO.findByUser(userId);
    }

    @Transactional(readOnly = true)
    public List<AccountHistory> findAccountHistoryByAccount (Long accountId) {
        return accountHistoryDAO.findAccountHistoryByAccount(accountId);
    }

    public void deleteAccount (Long accountId) throws AccountDoesntExistException {
        Account account = accountDAO.read(accountId);
        if (account == null) {
            throw exceptionFactoryServic.getAccountDoesntExistException(accountId);
        }
        accountDAO.delete(account);
    }

    public void createAccount (Account account) {
        accountDAO.create(account);
        AccountHistory accountHistory = new AccountHistory(
                account,
                account.getAmount(),
                AccountHistory.AccountOperation.CREATE
        );
        accountHistoryDAO.create(accountHistory);
    }

    public void depositAccount (Long accountId, BigDecimal amount) throws AccountDoesntExistException {
        if (!accountDAO.doDeposit(amount, accountId)) {
            throw exceptionFactoryServic.getAccountDoesntExistException(accountId);
        }
        Account account = accountDAO.read(accountId);

        AccountHistory accountHistory = new AccountHistory(
                account,
                amount,
                AccountHistory.AccountOperation.DEPOSIT
        );
        accountHistoryDAO.create(accountHistory);
    }

    public void transfer (Long accountId, BigDecimal amount, Long partnerAccountId)
            throws AccountDoesntExistException, AccountInsufficientFundsException {
        if (!accountDAO.doDebit(amount, accountId)) {
            throw exceptionFactoryServic.getAccountInsufficientFundsException(amount);
        }
        Account account = accountDAO.read(accountId);

        if (!accountDAO.doDeposit(amount, partnerAccountId)) {
            throw exceptionFactoryServic.getAccountDoesntExistException(accountId);
        }
        Account partnerAccount = accountDAO.read(partnerAccountId);

        AccountHistory accountHistory1 = new AccountHistory(
                account,
                amount,
                partnerAccount,
                AccountHistory.AccountOperation.TRANSFER
        );
        accountHistoryDAO.create(accountHistory1);

        AccountHistory accountHistory2 = new AccountHistory(
                partnerAccount,
                amount,
                account,
                AccountHistory.AccountOperation.DEPOSIT
        );
        accountHistoryDAO.create(accountHistory2);
    }

    public void debitAccount (Long accountId, BigDecimal amount)
            throws AccountDoesntExistException, AccountInsufficientFundsException {
        if (!accountDAO.doDebit(amount, accountId)) {
            throw exceptionFactoryServic.getAccountInsufficientFundsException(amount);
        }
        Account account = accountDAO.read(accountId);

        AccountHistory accountHistory = new AccountHistory(
                account,
                amount,
                AccountHistory.AccountOperation.DEBIT
        );
        accountHistoryDAO.create(accountHistory);
    }
}
