package com.effylab.bank.server.service;

import com.effylab.bank.server.dao.RoleDAO;
import com.effylab.bank.server.dao.UserDAO;
import com.effylab.bank.server.exception.UserAlreadyExistsException;
import com.effylab.bank.server.exception.UserDoesntExistException;
import com.effylab.bank.server.model.Role;
import com.effylab.bank.server.model.User;
import java.util.Collections;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import static org.springframework.transaction.annotation.Isolation.READ_COMMITTED;
import static org.springframework.transaction.annotation.Isolation.REPEATABLE_READ;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * <p>
 * @author Artem Labazin, xxlabaza@gmail.com
 * <p>
 * @since Sep 3, 2014
 * <p>
 * @version 1.0.0
 */
@Service
@Transactional(rollbackFor = Exception.class,
               isolation = READ_COMMITTED)
public class UserService {

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private RoleDAO roleDAO;

    @Autowired
    private ExceptionFactoryService exceptionFactoryServic;

    public Long createUser (User user) throws UserAlreadyExistsException {
        User dbUser = userDAO.getByUsername(user.getUsername());
        if (dbUser != null) {
            throw exceptionFactoryServic.getUserAlreadyExistsException(user.getUsername());
        }
        Role role = roleDAO.getByName("user");
        user.setRoles(Collections.singleton(role));
        user.setIsActive(Boolean.TRUE);
        return userDAO.create(user);
    }

    @Transactional(readOnly = true)
    public User getUserByUsername (String userName) {
        return userDAO.getByUsername(userName);
    }

    @Transactional(readOnly = true)
    public List<User> findAll () {
        return userDAO.findAll();
    }

    public void updateUserProfile (User user) throws UserDoesntExistException {
        User dbUser = userDAO.read(user.getId());
        if (dbUser == null) {
            throw exceptionFactoryServic.getUserDoesntExistException();
        }
        dbUser.setFirstName(user.getFirstName());
        dbUser.setMiddleName(user.getMiddleName());
        dbUser.setLastName(user.getLastName());
        userDAO.update(dbUser);
    }

    @Transactional(isolation = REPEATABLE_READ)
    public void deleteUser (String userName) throws UserDoesntExistException {
        if (!userDAO.doDeleteByUsername(userName)) {
            throw exceptionFactoryServic.getUserDoesntExistException();
        }
    }
}
