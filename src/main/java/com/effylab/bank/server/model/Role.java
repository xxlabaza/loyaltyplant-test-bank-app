package com.effylab.bank.server.model;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

@NamedQueries({
    @NamedQuery(name = "com.effylab.bank.server.model.Role.getByName",
                query = "FROM Role r " +
                        "WHERE r.name = :roleName")
})
@Entity
@Table(name = "`role`")
public class Role implements Serializable {

    private static final long serialVersionUID = -6451749081006208614L;

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "roles")
    private Set<User> users;

    public Role () {
    }

    public Long getId () {
        return id;
    }

    public void setId (Long id) {
        this.id = id;
    }

    public String getName () {
        return name;
    }

    public void setName (String name) {
        this.name = name;
    }

    public Set<User> getUsers () {
        return users;
    }

    public void setUsers (Set<User> users) {
        this.users = users;
    }

    @Override
    public String toString () {
        StringBuilder builder = new StringBuilder();
        builder.append("Role [");
        builder.append("id=").append(id);
        builder.append(", name=").append(name);
        builder.append("]");
        return builder.toString();
    }
}
