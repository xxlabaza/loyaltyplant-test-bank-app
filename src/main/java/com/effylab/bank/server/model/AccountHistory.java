package com.effylab.bank.server.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

@NamedQueries({
    @NamedQuery(name = "com.effylab.bank.server.model.AccountHistory.findAccountHistoryByAccount",
                query = "FROM AccountHistory ah " +
                        "WHERE ah.account.id = :accountId " +
                        "ORDER BY ah.time desc")
})
@Entity
@Table(name = "account_history")
public class AccountHistory implements Serializable {

    private static final long serialVersionUID = 3869129366406464832L;

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "account")
    private Account account;

    @Column(name = "amount")
    private BigDecimal amount;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "`time`")
    private Date time;

    @ManyToOne
    @JoinColumn(name = "partner_account")
    private Account partnerAccount;

    @Enumerated(EnumType.STRING)
    @Column(name = "`operation`")
    private AccountOperation operation;

    public AccountHistory () {
    }

    public AccountHistory (Account account, BigDecimal amount, Account partnerAccount, AccountOperation operation) {
        this.account = account;
        this.amount = amount;
        this.partnerAccount = partnerAccount;
        this.operation = operation;
    }

    public AccountHistory (Account account, BigDecimal amount, AccountOperation operation) {
        this.account = account;
        this.amount = amount;
        this.operation = operation;
    }

    public Long getId () {
        return id;
    }

    public void setId (Long id) {
        this.id = id;
    }

    public Account getAccount () {
        return account;
    }

    public void setAccount (Account account) {
        this.account = account;
    }

    public BigDecimal getAmount () {
        return amount;
    }

    public void setAmount (BigDecimal amount) {
        this.amount = amount;
    }

    public Date getTime () {
        return time;
    }

    public void setTime (Date time) {
        this.time = time;
    }

    public Account getPartnerAccount () {
        return partnerAccount;
    }

    public void setPartnerAccount (Account partnerAccount) {
        this.partnerAccount = partnerAccount;
    }

    public AccountOperation getOperation () {
        return operation;
    }

    public void setOperation (AccountOperation operation) {
        this.operation = operation;
    }

    @Override
    public String toString () {
        StringBuilder builder = new StringBuilder();
        builder.append("AccountHistory [");
        builder.append("account=").append(account);
        builder.append(", id=").append(id);
        builder.append(", amount=").append(amount);
        builder.append(", operation=").append(operation);
        builder.append(", partnerAccount=").append(partnerAccount);
        builder.append(", time=").append(time);
        builder.append("]");
        return builder.toString();
    }

    public static enum AccountOperation {

        CREATE,
        DEBIT, // снятие
        DEPOSIT,     // пополнение
        TRANSFER;
    }
}
