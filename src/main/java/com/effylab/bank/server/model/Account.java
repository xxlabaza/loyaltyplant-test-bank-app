package com.effylab.bank.server.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

@NamedQueries({
    @NamedQuery(name = "com.effylab.bank.server.model.Account.findByUser",
                query = "SELECT new Account(a.id, a.name, a.amount) " +
                        "FROM Account a " +
                        "WHERE a.user.id = :userId"),
    @NamedQuery(name = "com.effylab.bank.server.model.Account.doDebit",
                query = "UPDATE Account " +
                        "SET amount = (amount - :amount) " +
                        "WHERE id = :accountId AND (amount - :amount) >= 0"),
    @NamedQuery(name = "com.effylab.bank.server.model.Account.doDeposit",
                query = "UPDATE Account " +
                        "SET amount = (amount + :amount) " +
                        "WHERE id = :accountId")
})
@Entity
@Table(name = "account")
public class Account implements Serializable {

    private static final long serialVersionUID = -7533890659377419173L;

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "user")
    private User user;

    @Column(name = "name")
    private String name;

    @Column(name = "amount")
    private BigDecimal amount;

    @OneToMany(mappedBy = "account")
    private Set<AccountHistory> history;

    @OneToMany(mappedBy = "partnerAccount")
    private Set<AccountHistory> partnerIn;

    public Account () {
    }

    public Account (Long id, String name, BigDecimal amount) {
        this.id = id;
        this.name = name;
        this.amount = amount;
    }

    public Long getId () {
        return id;
    }

    public void setId (Long id) {
        this.id = id;
    }

    public User getUser () {
        return user;
    }

    public void setUser (User user) {
        this.user = user;
    }

    public String getName () {
        return name;
    }

    public void setName (String name) {
        this.name = name;
    }

    public BigDecimal getAmount () {
        return amount;
    }

    public void setAmount (BigDecimal amount) {
        this.amount = amount;
    }

    public Set<AccountHistory> getHistory () {
        return history;
    }

    public void setHistory (Set<AccountHistory> history) {
        this.history = history;
    }

    @Override
    public String toString () {
        StringBuilder builder = new StringBuilder();
        builder.append("Account [");
        builder.append("amount=").append(amount);
        builder.append(", id=").append(id);
        builder.append(", name=").append(name);
        builder.append("]");
        return builder.toString();
    }

    public Set<AccountHistory> getPartnerIn () {
        return partnerIn;
    }

    public void setPartnerIn (Set<AccountHistory> partnerIn) {
        this.partnerIn = partnerIn;
    }
}
