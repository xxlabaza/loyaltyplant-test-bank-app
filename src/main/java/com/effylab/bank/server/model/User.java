package com.effylab.bank.server.model;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

@NamedQueries({
    @NamedQuery(name = "com.effylab.bank.server.model.User.getByUsername",
                query = "SELECT u " +
                        "FROM User u " +
                        "LEFT JOIN FETCH u.accounts " +
                        "WHERE u.username = :username"),
    @NamedQuery(name = "com.effylab.bank.server.model.User.findAll",
                query = "FROM User u " +
                        "WHERE u.isActive = TRUE"),
    @NamedQuery(name = "com.effylab.bank.server.model.User.doDeleteByUsername",
                query = "DELETE FROM User " +
                        "WHERE username = :username")
})
@Entity
@Table(name = "`user`")
public class User implements Serializable {

    private static final long serialVersionUID = 2869607559695549544L;

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "middle_name")
    private String middleName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "is_active")
    private Boolean isActive;

    @OneToMany(mappedBy = "user")
    private Set<Account> accounts;

    @ManyToMany(cascade = CascadeType.MERGE)
    @JoinTable(name = "user_role",
               joinColumns = @JoinColumn(name = "user"),
               inverseJoinColumns = @JoinColumn(name = "role"))
    private Set<Role> roles;

    public User () {
    }

    public Long getId () {
        return id;
    }

    public void setId (Long id) {
        this.id = id;
    }

    public String getUsername () {
        return username;
    }

    public void setUsername (String username) {
        this.username = username;
    }

    public String getPassword () {
        return password;
    }

    public void setPassword (String password) {
        this.password = password;
    }

    public String getFirstName () {
        return firstName;
    }

    public void setFirstName (String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName () {
        return middleName;
    }

    public void setMiddleName (String middleName) {
        this.middleName = middleName;
    }

    public String getLastName () {
        return lastName;
    }

    public void setLastName (String lastName) {
        this.lastName = lastName;
    }

    public Boolean isIsActive () {
        return isActive;
    }

    public void setIsActive (Boolean isActive) {
        this.isActive = isActive;
    }

    public Set<Account> getAccounts () {
        return accounts;
    }

    public void setAccounts (Set<Account> accounts) {
        this.accounts = accounts;
    }

    public Set<Role> getRoles () {
        return roles;
    }

    public void setRoles (Set<Role> roles) {
        this.roles = roles;
    }

    @Override
    public String toString () {
        StringBuilder builder = new StringBuilder();
        builder.append("User [");
        builder.append("firstName=").append(firstName);
        builder.append(", id=").append(id);
        builder.append(", isActive=").append(isActive);
        builder.append(", lastName=").append(lastName);
        builder.append(", middleName=").append(middleName);
        builder.append(", password=").append(password);
        builder.append(", username=").append(username);
        builder.append("]");
        return builder.toString();
    }
}
