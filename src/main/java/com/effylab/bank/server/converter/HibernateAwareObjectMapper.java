package com.effylab.bank.server.converter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.hibernate4.Hibernate4Module;

public class HibernateAwareObjectMapper extends ObjectMapper {

    private static final long serialVersionUID = 4700165395892237592L;

    public HibernateAwareObjectMapper () {
        registerModule(new Hibernate4Module());
    }
}
