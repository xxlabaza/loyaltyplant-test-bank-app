package com.effylab.bank.server.controller;

import com.effylab.bank.server.exception.UserAlreadyExistsException;
import com.effylab.bank.server.model.User;
import com.effylab.bank.server.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * <p>
 * @author Artem Labazin, xxlabaza@gmail.com
 * <p>
 * @since Aug 22, 2014
 * <p>
 * @version 1.0.0
 */
@Controller
public class AuthenticationController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String registration (@ModelAttribute User user, BindingResult result,
                                RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            return "/registration";
        }
        try {
            userService.createUser(user);
            redirectAttributes.addFlashAttribute("successMessage",
                                                 "User has been successfully registered.");
        } catch (UserAlreadyExistsException ex) {
            redirectAttributes.addFlashAttribute("errorMessage", ex.getMessage());
        }
        return "redirect:/login";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String logIn (ModelMap model,
                         @RequestParam(value = "error",
                                       required = false) String error,
                         @RequestParam(value = "logout",
                                       required = false) String logout,
                         @ModelAttribute("errorMessage") String errorMessage,
                         @ModelAttribute("successMessage") String successMessage) {
        if (error != null) {
            model.addAttribute("errorMessage", "Invalid username and password!");
        }
        if (logout != null) {
            model.addAttribute("successMessage", "You've been logged out successfully.");
        }
        if (!errorMessage.isEmpty()) {
            model.addAttribute("errorMessage", errorMessage);
        }
        if (!successMessage.isEmpty()) {
            model.addAttribute("successMessage", successMessage);
        }
        return "login";
    }
}
