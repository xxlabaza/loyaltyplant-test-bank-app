package com.effylab.bank.server.controller;

import com.effylab.bank.server.exception.AccountDoesntExistException;
import com.effylab.bank.server.exception.AccountInsufficientFundsException;
import com.effylab.bank.server.exception.UserDoesntExistException;
import com.effylab.bank.server.model.Account;
import com.effylab.bank.server.model.AccountHistory;
import com.effylab.bank.server.model.User;
import com.effylab.bank.server.service.AccountService;
import com.effylab.bank.server.service.UserService;
import java.math.BigDecimal;
import java.security.Principal;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * <p>
 * @author Artem Labazin, xxlabaza@gmail.com
 * <p>
 * @since Aug 22, 2014
 * <p>
 * @version 1.0.0
 */
@Controller
public class AccountsController {

    @Autowired
    private UserService userService;

    @Autowired
    private AccountService accountService;

    @RequestMapping(value = "/accounts", method = RequestMethod.GET)
    public String showUserAccounts (ModelMap model, Principal principal,
                                    @ModelAttribute("errorMessage") String errorMessage,
                                    @ModelAttribute("successMessage") String successMessage) {
        String userName = principal.getName();
        User currentUser = userService.getUserByUsername(userName);
        List<User> allUsers = userService.findAll();
        model.addAttribute("currentUser", currentUser);
        model.addAttribute("otherUsers", allUsers);
        if (!errorMessage.isEmpty()) {
            model.addAttribute("errorMessage", errorMessage);
        }
        if (!successMessage.isEmpty()) {
            model.addAttribute("successMessage", successMessage);
        }
        return "accounts";
    }

    @RequestMapping(value = "/accounts/edit_user", method = RequestMethod.POST)
    public String updateUserProfile (@ModelAttribute User user, RedirectAttributes redirectAttributes) {
        try {
            userService.updateUserProfile(user);
            redirectAttributes.addFlashAttribute("successMessage", "User's profile successfully updated.");
        } catch (UserDoesntExistException ex) {
            redirectAttributes.addFlashAttribute("errorMessage", ex.getMessage());
        }
        return "redirect:/accounts";
    }

    @RequestMapping(value = "/accounts/add_new", method = RequestMethod.POST)
    public String createNewAccount (@ModelAttribute Account account, Principal principal,
                                    RedirectAttributes redirectAttributes) {
        String userName = principal.getName();
        User currentUser = userService.getUserByUsername(userName);
        account.setUser(currentUser);
        accountService.createAccount(account);
        redirectAttributes.addFlashAttribute("successMessage", "Account has been successfully created.");
        return "redirect:/accounts";
    }

    @RequestMapping(value = "/accounts/deposit", method = RequestMethod.POST)
    public String deposit (@RequestParam("account") Long accountId, @RequestParam("amount") BigDecimal amount,
                           RedirectAttributes redirectAttributes) {
        try {
            accountService.depositAccount(accountId, amount);
            redirectAttributes.addFlashAttribute("successMessage", "Account has been successfully deposited.");
        } catch (AccountDoesntExistException ex) {
            redirectAttributes.addFlashAttribute("errorMessage", ex.getMessage());
        }
        return "redirect:/accounts";
    }

    @RequestMapping(value = "/accounts/transfer", method = RequestMethod.POST)
    public String transfer (@RequestParam("account") Long accountId, @RequestParam("amount") BigDecimal amount,
                            @RequestParam("partnerAccount") Long partnerAccountId, RedirectAttributes redirectAttributes) {
        try {
            accountService.transfer(accountId, amount, partnerAccountId);
            redirectAttributes.addFlashAttribute("successMessage", "Money has been successfully transfered.");
        } catch (AccountDoesntExistException | AccountInsufficientFundsException ex) {
            redirectAttributes.addFlashAttribute("errorMessage", ex.getMessage());
        }
        return "redirect:/accounts";
    }

    @RequestMapping(value = "/accounts/debit", method = RequestMethod.POST)
    public String debit (@RequestParam("account") Long accountId, @RequestParam("amount") BigDecimal amount,
                         RedirectAttributes redirectAttributes) {
        try {
            accountService.debitAccount(accountId, amount);
            redirectAttributes.addFlashAttribute("successMessage", "Money has been successfully debited.");
        } catch (AccountDoesntExistException | AccountInsufficientFundsException ex) {
            redirectAttributes.addFlashAttribute("errorMessage", ex.getMessage());
        }
        return "redirect:/accounts";
    }

    @RequestMapping(value = "/accounts/{accountId}", method = RequestMethod.GET)
    public String delete (@PathVariable("accountId") Long accountId, RedirectAttributes redirectAttributes) {
        try {
            accountService.deleteAccount(accountId);
            redirectAttributes.addFlashAttribute("successMessage", "Account has been successfully deleted.");
        } catch (AccountDoesntExistException ex) {
            redirectAttributes.addFlashAttribute("errorMessage", ex.getMessage());
        }
        return "redirect:/accounts";
    }

    @RequestMapping(value = "/accounts/user/{userId}/all", method = RequestMethod.GET,
                    produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Account> findAccountsByUser (@PathVariable Long userId) {
        return accountService.findAccountByUser(userId);
    }

    @RequestMapping(value = "/accounts/{accountId}/history", method = RequestMethod.GET,
                    produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<AccountHistory> findAccountHistoryByAccount (@PathVariable Long accountId) {
        return accountService.findAccountHistoryByAccount(accountId);
    }

    @RequestMapping(value = "/accounts/user/delete", method = RequestMethod.GET)
    public String deleteUser (Principal principal, RedirectAttributes redirectAttributes) {
        String username = principal.getName();
        try {
            userService.deleteUser(username);
            redirectAttributes.addFlashAttribute("successMessage",
                                                 "User has been successfully deleted.");
        } catch (UserDoesntExistException ex) {
            redirectAttributes.addFlashAttribute("errorMessage", ex.getMessage());
        }
        return "redirect:/login";
    }
}
