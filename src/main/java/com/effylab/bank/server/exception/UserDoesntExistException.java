package com.effylab.bank.server.exception;

public class UserDoesntExistException extends Exception {

    private static final long serialVersionUID = 2657979776113522782L;

    public UserDoesntExistException (String message) {
        super(message);
    }
}
