package com.effylab.bank.server.exception;

public class AccountDoesntExistException extends Exception {

    private static final long serialVersionUID = -608598660216006321L;

    public AccountDoesntExistException (String message) {
        super(message);
    }
}
