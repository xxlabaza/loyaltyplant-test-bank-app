package com.effylab.bank.server.exception;

public class AccountInsufficientFundsException extends Exception {

    private static final long serialVersionUID = 1067777909566007597L;

    public AccountInsufficientFundsException (String message) {
        super(message);
    }
}
