package com.effylab.bank.server.exception;

public class UserAlreadyExistsException extends Exception {

    private static final long serialVersionUID = -7875278074530964255L;

    public UserAlreadyExistsException (String message) {
        super(message);
    }
}
