package com.effylab.bank.server.aop;

import org.springframework.aop.support.DefaultIntroductionAdvisor;

public class FindIntroductionAdvisor extends DefaultIntroductionAdvisor {

    private static final long serialVersionUID = 3659326941137124926L;

    public FindIntroductionAdvisor () {
        super(new FindIntroductionInterceptor());
    }
}
