package com.effylab.bank.server.aop;

import com.effylab.bank.server.dao.fetch.FetchExecutor;
import org.aopalliance.intercept.MethodInvocation;
import org.springframework.aop.IntroductionInterceptor;

/**
 *
 * <p>
 * @author Artem Labazin, xxlabaza@gmail.com
 * <p>
 * @since Aug 27, 2014
 * <p>
 * @version 1.0.0
 */
public class FindIntroductionInterceptor implements IntroductionInterceptor {

    @Override
    public Object invoke (MethodInvocation methodInvocation) throws Throwable {
        FetchExecutor finderExecutor = (FetchExecutor) methodInvocation.getThis();
        String methodName = methodInvocation.getMethod().getName();
        if (methodName.startsWith("find")) {
            Object[] arguments = methodInvocation.getArguments();
            return finderExecutor.executeFind(methodInvocation.getMethod(), arguments);
        } else if (methodName.startsWith("get")) {
            Object[] arguments = methodInvocation.getArguments();
            return finderExecutor.executeGet(methodInvocation.getMethod(), arguments);
        } else if (methodName.startsWith("do")) {
            Object[] arguments = methodInvocation.getArguments();
            return finderExecutor.executeDo(methodInvocation.getMethod(), arguments);
        } else {
            return methodInvocation.proceed();
        }
    }

    @Override
    public boolean implementsInterface (Class<?> type) {
        return type.isInterface() && FetchExecutor.class.isAssignableFrom(type);
    }
}
